export { default as CategoryCloud } from '../../components/CategoryCloud.vue'
export { default as DateInfoPost } from '../../components/DateInfoPost.vue'
export { default as FullwithContainer } from '../../components/FullwithContainer.vue'
export { default as Logo } from '../../components/Logo.vue'
export { default as MainPost } from '../../components/MainPost.vue'
export { default as PostItem } from '../../components/PostItem.vue'
export { default as IconClose } from '../../components/icons/IconClose.vue'
export { default as IconDownArrow } from '../../components/icons/IconDownArrow.vue'
export { default as IconPlus } from '../../components/icons/IconPlus.vue'

export const LazyCategoryCloud = import('../../components/CategoryCloud.vue' /* webpackChunkName: "components/CategoryCloud" */).then(c => c.default || c)
export const LazyDateInfoPost = import('../../components/DateInfoPost.vue' /* webpackChunkName: "components/DateInfoPost" */).then(c => c.default || c)
export const LazyFullwithContainer = import('../../components/FullwithContainer.vue' /* webpackChunkName: "components/FullwithContainer" */).then(c => c.default || c)
export const LazyLogo = import('../../components/Logo.vue' /* webpackChunkName: "components/Logo" */).then(c => c.default || c)
export const LazyMainPost = import('../../components/MainPost.vue' /* webpackChunkName: "components/MainPost" */).then(c => c.default || c)
export const LazyPostItem = import('../../components/PostItem.vue' /* webpackChunkName: "components/PostItem" */).then(c => c.default || c)
export const LazyIconClose = import('../../components/icons/IconClose.vue' /* webpackChunkName: "components/icons/IconClose" */).then(c => c.default || c)
export const LazyIconDownArrow = import('../../components/icons/IconDownArrow.vue' /* webpackChunkName: "components/icons/IconDownArrow" */).then(c => c.default || c)
export const LazyIconPlus = import('../../components/icons/IconPlus.vue' /* webpackChunkName: "components/icons/IconPlus" */).then(c => c.default || c)
